package dijkstra.main.model;

public class Vertex {

	private String name;
	private Double distance;
	private Vertex predecessor;

	public Vertex(String name, Double distance, Vertex predecessor) {
		super();
		this.name = name;
		this.distance = distance;
		this.predecessor = predecessor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Vertex getPredecessor() {
		return predecessor;
	}

	public void setPredecessor(Vertex predecessor) {
		this.predecessor = predecessor;
	}

	@Override
	public boolean equals(Object o) {

		if (o == this)
			return true;
		if (!(o instanceof Vertex))
			return false;

		Vertex v = (Vertex) o;
		return v.name.equals(this.name);
	}

	@Override
	public int hashCode() {
		return 31 * 17 + name.hashCode();
	}

	@Override
	public String toString() {
		return "Vertex [name=" + name + ", distance=" + distance + ", predecessor=" + predecessor + "]";
	}

}
