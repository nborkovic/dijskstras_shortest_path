package dijkstra.main.model;

public class Edge {

	private Vertex startVertex;
	private Vertex endVertex;
	private Double weight;

	public Edge(Vertex startVertex, Vertex endVertex, Double weight) {
		super();
		this.startVertex = startVertex;
		this.endVertex = endVertex;
		this.weight = weight;
	}

	public Vertex getStartVertex() {
		return startVertex;
	}

	public void setStartVertex(Vertex startVertex) {
		this.startVertex = startVertex;
	}

	public Vertex getEndVertex() {
		return endVertex;
	}

	public void setEndVertex(Vertex endVertex) {
		this.endVertex = endVertex;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	/**
	 * Edges are equal if they have the same name starting and ending vertex.
	 */
	@Override
	public boolean equals(Object o) {

		if (o == this)
			return true;
		if (!(o instanceof Edge))
			return false;

		Edge e = (Edge) o;
		return e.startVertex.equals(this.startVertex) && e.endVertex.equals(this.endVertex);
	}

	@Override
	public int hashCode() {
		return 31 * 17 + this.startVertex.hashCode() + this.endVertex.hashCode();
	}

	@Override
	public String toString() {
		return "Edge [startVertex=" + startVertex + ", endVertex=" + endVertex + ", weight=" + weight + "]";
	}

}
