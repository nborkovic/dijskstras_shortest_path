package dijkstra.main.model;

import java.util.HashSet;
import java.util.Set;

public class Graph {

	private Set<Vertex> vertices = new HashSet<Vertex>();
	private Set<Edge> edges = new HashSet<Edge>();

	public Graph(Set<Vertex> vertices, Set<Edge> edges) {
		super();
		this.vertices = vertices;
		this.edges = edges;
	}

	/**
	 * Get all adjacent vertices from Vertex u. Adjacent vertices are vertices in
	 * which we can go from current vertex by edge.
	 * 
	 * @param u
	 * @return
	 */
	public Set<Vertex> getAdjacentVertices(Vertex u) {
		Set<Vertex> adjacentVertices = new HashSet<Vertex>();

		for (Vertex v : this.vertices) {
			if (v.equals(u))
				continue;

			if (edgeExists(u, v))
				adjacentVertices.add(v);

		}
		return adjacentVertices;
	}

	/**
	 * If vertex v2 is adjacent from v1 then surely exist one edge that connects
	 * them. Edge have direction, so it does matter which of vertices is first and
	 * which is second.
	 *
	 * @param v1
	 * @param v2
	 * @return true if edge exists
	 * @return false if edge doesn't exist
	 */
	private boolean edgeExists(Vertex v1, Vertex v2) {

		for (Edge e : this.edges) {
			if (e.getStartVertex().getName().equals(v1.getName()) && e.getEndVertex().getName().equals(v2.getName()))
				return true;
		}
		return false;
	}

	/**
	 * Get edge weight between two vertices. It's important which vertex is first
	 * and which is second, because two vertices can have two edges (one per
	 * direction). So v1 >> v2 is not the same as v2 >> v1
	 * 
	 * @param v1
	 * @param v2
	 * @return weight if exists, else null
	 */
	public Double getEdgeWeight(Vertex v1, Vertex v2) {

		for (Edge e : this.edges) {
			if (e.getStartVertex().getName().equals(v1.getName()) && e.getEndVertex().getName().equals(v2.getName()))
				return e.getWeight();
		}
		return null;
	}

	public Set<Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(Set<Vertex> vertices) {
		this.vertices = vertices;
	}

	public Set<Edge> getEdges() {
		return edges;
	}

	public void setEdges(Set<Edge> edges) {
		this.edges = edges;
	}

	@Override
	public String toString() {
		String output = "Graph: \n";
		for (Vertex v : this.vertices) {
			output += v.toString() + "\n";
		}
		for (Edge e : this.edges) {
			output += e.toString() + "\n";
		}
		return output;

	}

}
