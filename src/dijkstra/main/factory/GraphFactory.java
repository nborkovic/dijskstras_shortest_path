package dijkstra.main.factory;

import java.util.HashSet;
import java.util.Set;

import dijkstra.main.model.Edge;
import dijkstra.main.model.Graph;
import dijkstra.main.model.Vertex;

public class GraphFactory {
	
	
	
	
	public Graph createNewGraph() {
		
		
		Set<Vertex> vertices = new HashSet<Vertex>();
		Set<Edge> edges = new HashSet<Edge>();
		
		Vertex a = new Vertex("a", null, null);
		Vertex b = new Vertex("b", null, null);
		Vertex c = new Vertex("c", null, null);
		Vertex d = new Vertex("d", null, null);
		Vertex e = new Vertex("e", null, null);
		Vertex f = new Vertex("f", null, null);
		Vertex g = new Vertex("g", null, null);
		Vertex h = new Vertex("h", null, null);
		Vertex i = new Vertex("i", null, null);
		Vertex j = new Vertex("j", null, null);
		Vertex k = new Vertex("k", null, null);
		Vertex l = new Vertex("l", null, null);
		Vertex m = new Vertex("m", null, null);
		Vertex n = new Vertex("n", null, null);
		Vertex o = new Vertex("o", null, null);
		Vertex p = new Vertex("p", null, null);
		Vertex r = new Vertex("r", null, null);
		Vertex s = new Vertex("s", null, null);
		
		vertices.add(a);
		vertices.add(b);
		vertices.add(c);
		vertices.add(d);
		vertices.add(e);
		vertices.add(f);
		vertices.add(g);
		vertices.add(h);
		vertices.add(i);
		vertices.add(j);
		vertices.add(k);
		vertices.add(l);
		vertices.add(m);
		vertices.add(n);
		vertices.add(o);
		vertices.add(p);
		vertices.add(r);
		vertices.add(s);
		
		Edge e1  = new Edge(a, b, 2.0 );
		Edge e2  = new Edge(a, c, 6.0 );
		Edge e3  = new Edge(a, d, 12.0);
		Edge e4  = new Edge(b, e, 9.0 );
		Edge e5  = new Edge(b, c, 3.0 );
		Edge e6  = new Edge(c, f, 1.0 );
		Edge e7  = new Edge(d, h, 2.0 );				
		Edge e8  = new Edge(e, g, 1.0 );
		Edge e9  = new Edge(f, e, 2.0 );
		Edge e10 = new Edge(f, h, 4.0 );
		Edge e11 = new Edge(g, i, 2.0 );
		Edge e12 = new Edge(h, i, 3.0 );
		Edge e13 = new Edge(h, k, 1.0 );
		Edge e14 = new Edge(h, l, 3.0 );
		Edge e15 = new Edge(h, m, 2.0 );
		Edge e16 = new Edge(i, j, 1.0 );
		Edge e17 = new Edge(j, n, 5.0 );
		Edge e18 = new Edge(k, j, 5.0 );
		Edge e19 = new Edge(k, o, 9.0 );
		Edge e20 = new Edge(l, p, 7.0 );
		Edge e21 = new Edge(m, l, 2.0 );
		Edge e22 = new Edge(m, r, 2.0 );
		Edge e23 = new Edge(n, o, 7.0 );
		Edge e24 = new Edge(n, s, 9.0 );
		Edge e25 = new Edge(o, s, 11.0);
		Edge e26 = new Edge(p, s, 6.0 );
		Edge e27 = new Edge(r, s, 3.0 );
		
		edges.add(e1);
		edges.add(e2);
		edges.add(e3);
		edges.add(e4);
		edges.add(e5);
		edges.add(e6);
		edges.add(e7);
		edges.add(e8);
		edges.add(e9);
		edges.add(e10);
		edges.add(e11);
		edges.add(e12);
		edges.add(e13);
		edges.add(e14);
		edges.add(e15);
		edges.add(e16);
		edges.add(e17);
		edges.add(e18);
		edges.add(e19);
		edges.add(e20);
		edges.add(e21);
		edges.add(e22);
		edges.add(e23);
		edges.add(e24);
		edges.add(e25);
		edges.add(e26);
		edges.add(e27);

		
		Graph graph = new Graph(vertices, edges);
		return graph;
		
	}

}
