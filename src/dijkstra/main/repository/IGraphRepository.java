package dijkstra.main.repository;

import java.io.FileNotFoundException;
import java.io.IOException;

import dijkstra.main.model.Graph;

public interface IGraphRepository {
	
	void saveGraph(Graph graph) throws IOException;
	Graph loadGraph(String filename) throws FileNotFoundException;

}
