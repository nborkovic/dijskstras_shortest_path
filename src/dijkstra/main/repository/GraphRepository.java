package dijkstra.main.repository;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import dijkstra.main.model.Edge;
import dijkstra.main.model.Graph;
import dijkstra.main.model.Vertex;
import dijkstra.main.repository.IGraphRepository;

public class GraphRepository implements IGraphRepository {


	@Override
	public void saveGraph(Graph graph) throws IOException {

		StringBuilder sb = new StringBuilder();

		// first row
		sb.append("S");
		for (Vertex v : graph.getVertices()) {
			sb.append("," + v.getName());
		}
		sb.append("\n");

		for (Vertex v1 : graph.getVertices()) {
			sb.append(v1.getName());
			for (Vertex v2 : graph.getVertices()) {
				Double w = graph.getEdgeWeight(v1, v2);
				String weight = (w != null) ? Double.toString(w) : "X";
				sb.append("," + weight);

			}
			sb.append("\n");
		}
		String fileName = "SavedGraphs/Graph.csv";
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		writer.write(sb.toString());
		writer.close();
	}
	
	

	@Override
	public Graph loadGraph(String fileName) throws FileNotFoundException {

//		String fileName = "SavedGraphs/Graph.csv";
		File file = new File(fileName);
		BufferedReader br = new BufferedReader(new FileReader(file));

		
		String line;
		String [] vertexNames = null;
		int rowCounter = 0;
		Set<Vertex> vertices = new HashSet<Vertex>();
		Set<Edge> edges = new HashSet<Edge>();
		
		
		try {
			while ((line = br.readLine()) != null) {
				String[] splitLine = line.split(",");
				
				//get vertices from first line		
				if (splitLine[0].equals("S")) {
					vertexNames = splitLine;
					for (int i = 1; i < splitLine.length; ++i) {
						vertices.add(new Vertex(splitLine[i], null, null));
					}
					rowCounter  ++;
					continue;
				}
				
				//get edge weights from matrix
				for (int i=1; i< splitLine.length; ++i) {
					String weight = splitLine[i]; 
					if (weight.equals("X")) {
						continue;
					}
					Vertex v1 = new Vertex(vertexNames[rowCounter], null, null);
					Vertex v2 = new Vertex(vertexNames[i], null, null);
					edges.add(new Edge(v1, v2, Double.parseDouble(weight)));
				}
				
				rowCounter  ++;
			}			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new Graph(vertices, edges);
	}

}
