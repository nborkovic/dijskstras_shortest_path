package dijkstra.main.algorithm;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import dijkstra.main.model.Graph;
import dijkstra.main.model.Vertex;

public class DijkstrasAlgorithm {

	private Graph graph;
	private Vertex startVertex;
	private Vertex endVertex;
	private Set<Vertex> processedSet;
	private Set<Vertex> remainingSet;

	public DijkstrasAlgorithm(Graph graph, Vertex startVertex, Vertex endingVertex) {
		super();
		this.graph = graph;
		this.startVertex = startVertex;
		this.endVertex = endingVertex;
	}

	/**
	 * Starting point of algorithm. Result of algorithm will be set processedSet.
	 */
	public void run() {
		initializeSingleSource();
		initializeSets();

		while (!remainingSet.isEmpty()) {
			Vertex u = extractMinDistaceVertex();
			processedSet.add(u);
			for (Vertex v : graph.getAdjacentVertices(u)) {
				relax(u, v);
			}
		}
	}

	/**
	 * Initialize sets for algorithm
	 */
	private void initializeSets() {
		this.processedSet = new HashSet<Vertex>();
		this.remainingSet = this.graph.getVertices();
	}

	/**
	 * Set all vertices to initial state for algorithm
	 */
	private void initializeSingleSource() {
		for (Vertex v : this.graph.getVertices()) {
			v.setDistance(Double.MAX_VALUE);
			v.setPredecessor(null);
		}
		this.startVertex.setDistance((double) 0);
	}

	/**
	 * Extracts vertex with minimal distance from starting vertex.
	 * 
	 * @return
	 */
	private Vertex extractMinDistaceVertex() {
		Vertex minDistVertex = Collections.min(remainingSet, Comparator.comparing(v -> v.getDistance()));
		remainingSet.remove(minDistVertex);
		return minDistVertex;
	}

	/**
	 * This method updates provided vertices u and v.
	 * 
	 * @param u
	 * @param v
	 */
	private void relax(Vertex u, Vertex v) {

		Double w = graph.getEdgeWeight(u, v);
		if (v.getDistance() > u.getDistance() + w) {
			v.setDistance(u.getDistance() + w);
			v.setPredecessor(u);
		}
	}

	/**
	 * Recursive function that reaches all predecessor names of one vertex
	 * 
	 * @param v
	 * @return
	 */
	private String getPredecessorNames(Vertex v) {

		if (v.getPredecessor() == null) {
			return v.getName();
		}
		return v.getName() + " <- " + getPredecessorNames(v.getPredecessor());

	}

	/**
	 * Finalize function of Dijkstra's algorithm.It represents what algorithm
	 * calculated.
	 * 
	 * @return Shortest paths to all vertices in graph and total distances to them.
	 */
	public String getAllShortestPaths() {
		StringBuilder sb = new StringBuilder();
		String tableHeader = "      Distance      |            Path\n"
				+ "____________________|_____________________________\n";

		sb.append("==================================================\n");
		sb.append("\nStarting vertex: " + this.startVertex.getName() + "\n");
		sb.append(this.endVertex != null ? "Ending vertex:   " + this.endVertex.getName() + "\n\n\n"
				: "No ending vertex provided!\n\n\n");
		sb.append(tableHeader);

		if (endVertex == null) {
			for (Vertex v : processedSet) {
				if (v.getDistance().equals(Double.MAX_VALUE)) {
					sb.append(String.format("%-20s|", " -- Not adjacent --") + " " + getPredecessorNames(v) + "\n");
				} else {
					sb.append(String.format("%-20g|", v.getDistance()) + " " + getPredecessorNames(v) + "\n");
				}
			}
		} else {
			for (Vertex v : processedSet) {
				if (v.getName().equals(endVertex.getName())) {
					if (v.getDistance().equals(Double.MAX_VALUE)) {
						sb.append(String.format("%-20s|", " -- Not adjacent --") + " " + getPredecessorNames(v) + "\n");
					} else {
						sb.append(String.format("%-20g|", v.getDistance()) + " " + getPredecessorNames(v) + "\n");
					}
				}
			}
		}
		sb.append("\n==================================================\n");
		return sb.toString();
	}

}
