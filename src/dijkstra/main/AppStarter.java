package dijkstra.main;

import java.io.IOException;
import java.util.Scanner;

import dijkstra.main.algorithm.DijkstrasAlgorithm;
import dijkstra.main.model.Graph;
import dijkstra.main.model.Vertex;
import dijkstra.main.repository.GraphRepository;

public class AppStarter {

	public static void main(String[] args){

		System.out.println("[ INFO ] Starting program!");

		
		GraphRepository repo = new GraphRepository();
		Scanner scanner = new Scanner(System.in);
		Graph loadedGraph = null;
		Vertex startingVertex = null;
		Vertex endingVertex = null;

		
//		Uncomment this 4 lines below to create and save graph example if you don't have one	
		
// 		import dijkstra.main.factory.GraphFactory;
//		GraphFactory factory = new GraphFactory();
//		Graph g = factory.createNewGraph();
//		repo.saveGraph(g);

		
		
		// Getting full path to graph from user
		while (true) {
			System.out.println("Please provide full path to your graph.");
			String fullPathToGraph = scanner.nextLine();

			try {
				System.out.println("[ INFO ] Loading graph from file: " + fullPathToGraph);
				loadedGraph = repo.loadGraph(fullPathToGraph);
				break;
			} catch (IOException e) {
				System.out.println("[ EXCEPTION ] Cannot find '" + fullPathToGraph +"'. Please try again!\n ");
			}

		}

		// Getting starting vertex from user
		while (true) {
			System.out.println("Please select the starting vertex.");
			String startingVertexName = scanner.nextLine();
			for (Vertex v : loadedGraph.getVertices()) {
				if (v.getName().equals(startingVertexName)) {
					startingVertex = v;
				}
			}

			if (startingVertex == null) {
				System.out.println("[ EXCEPTION ] Provided vertex name '" +startingVertexName+"' does not exist. Try again! ");
			} else {
				break;
			}
		}
		
		
		// Getting ending vertex from user
		System.out.println("Please select the ending vertex. (Optional)");
		String endingVertexName = scanner.nextLine();
		for (Vertex v : loadedGraph.getVertices()) {
			if (v.getName().equals(endingVertexName)) {
				endingVertex = v;
			}
		}

		if (endingVertex == null) {
			System.out.println("Getting all shortest paths from starting vertex.");
		} else {
			System.out.println("Getting shortest path from starting vertex "+ startingVertex.getName() +" to ending vertex: " + endingVertex.getName());
		}
		

		DijkstrasAlgorithm algorithm = new DijkstrasAlgorithm(loadedGraph, startingVertex, endingVertex);
		algorithm.run();
		System.out.println(algorithm.getAllShortestPaths());	
		scanner.close();
	}
}
